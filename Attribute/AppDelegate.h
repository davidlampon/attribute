//
//  AppDelegate.h
//  Attribute
//
//  Created by David Lampon Diestre on 28/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
